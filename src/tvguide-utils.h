/**
 * TV Guide
 * Copyright (c) 2017-2018 Jan-Michael Brummer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TVGUIDE_UTILS_H
#define TVGUIDE_UTILS_H

#define gtk_widget_set_margin(widget, x1, y1, x2, y2) { \
		gtk_widget_set_margin_top(widget, y1); \
		gtk_widget_set_margin_bottom(widget, y2); \
		gtk_widget_set_margin_start(widget, x1); \
		gtk_widget_set_margin_end(widget, x2); \
}

#define ui_set_suggested_style(widget) gtk_style_context_add_class(gtk_widget_get_style_context(widget), GTK_STYLE_CLASS_SUGGESTED_ACTION)


gchar *tvguide_read_url(gchar * url, gsize * len);
void tvguide_box_header_func(GtkListBoxRow *row, GtkListBoxRow *before, gpointer user_data);
gboolean tvguide_strv_contains(const gchar * const *strv, const gchar *str);
gchar **tvguide_strv_add(gchar **strv, const gchar *str);
void tvguide_save_key_file (GKeyFile *file);

#endif
